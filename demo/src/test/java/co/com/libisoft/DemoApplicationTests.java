package co.com.libisoft;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import co.com.libisoft.entity.UsuariosEntity;
import co.com.libisoft.repository.IUsuarioRepo;

@SpringBootTest
class DemoApplicationTests {

	@Autowired
	private IUsuarioRepo repo;
	
	@Autowired
	private BCryptPasswordEncoder pwsEncoder;
	
	@Test
	public void crearUsuarioTest() {
		UsuariosEntity us=new UsuariosEntity(1,"admin",pwsEncoder.encode("123qwe"),null);
		UsuariosEntity usr=repo.save(us);
		assertTrue(usr.getPassword().equalsIgnoreCase(us.getPassword()));
	}

}
