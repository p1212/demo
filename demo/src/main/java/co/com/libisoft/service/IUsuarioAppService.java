package co.com.libisoft.service;

import java.util.List;

import co.com.libisoft.dto.usuario.UsuarioDTO;
import co.com.libisoft.entity.UsuariosEntity;

public interface IUsuarioAppService {
	public UsuarioDTO nuevoUsuario(UsuarioDTO usuario);
	public UsuarioDTO actualizarUsuario(UsuarioDTO usuario);
	public List<UsuariosEntity> buscarUsuarios();
	public UsuarioDTO buscarUsuarioNombre(String usuario);
	public UsuarioDTO buscarUsuarioNombre(long idUsuario);
}
