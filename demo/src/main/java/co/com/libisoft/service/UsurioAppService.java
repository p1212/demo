package co.com.libisoft.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import co.com.libisoft.dto.usuario.UsuarioDTO;
import co.com.libisoft.entity.UsuariosEntity;
import co.com.libisoft.repository.IUsuarioRepo;

@Service
public class UsurioAppService implements IUsuarioAppService {
	@Autowired
	private IUsuarioRepo usuarioRepo;
	@Autowired
	private BCryptPasswordEncoder pwsEncoder;
	
	@Override
	public UsuarioDTO nuevoUsuario(UsuarioDTO usuario) {
		UsuariosEntity usr=usuarioRepo.save(new UsuariosEntity(usuario.getId(),usuario.getUsuario(),pwsEncoder.encode(usuario.getPassword()),usuario.getFechaIngreso()));
		return new UsuarioDTO(usr.getId(),usr.getUsuario(),usr.getPassword(),usr.getFechaIngreso());
	}

	@Override
	public UsuarioDTO actualizarUsuario(UsuarioDTO usuario) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<UsuariosEntity> buscarUsuarios() {
		return usuarioRepo.findAll();
	}

	@Override
	public UsuarioDTO buscarUsuarioNombre(String usuario) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UsuarioDTO buscarUsuarioNombre(long idUsuario) {
		// TODO Auto-generated method stub
		return null;
	}

}
