package co.com.libisoft.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import co.com.libisoft.entity.UsuariosEntity;

public interface IUsuarioRepo  extends JpaRepository<UsuariosEntity, Long>{

	UsuariosEntity findByUsuario(String usuario);
}
