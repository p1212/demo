package co.com.libisoft.dto.usuario;

import java.util.Date;

public class UsuarioDTO {

	private long id;
	private String usuario;
	private String password;
	private Date fechaIngreso;
	
	
	public UsuarioDTO() {
	}


	public UsuarioDTO(long id, String usuario, String password, Date fechaIngreso) {
		super();
		this.id = id;
		this.usuario = usuario;
		this.password = password;
		this.fechaIngreso = fechaIngreso;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getUsuario() {
		return usuario;
	}


	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public Date getFechaIngreso() {
		return fechaIngreso;
	}


	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	
	
}
