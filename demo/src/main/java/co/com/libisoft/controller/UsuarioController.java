package co.com.libisoft.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import co.com.libisoft.dto.usuario.UsuarioDTO;
import co.com.libisoft.repository.IUsuarioRepo;
import co.com.libisoft.service.IUsuarioAppService;

@Controller
public class UsuarioController {

	@Autowired
	private IUsuarioAppService usuarioAppService;
	
	@RequestMapping("/Usuarios")
    public ModelAndView usuarios(ModelAndView modelAndView) {
        modelAndView.setViewName("admin/pages/Usuario/Usuarios");
        modelAndView.addObject("usuarios", usuarioAppService.buscarUsuarios());
        return modelAndView;
    }
	
	@RequestMapping("/UsuariosAdd")
    public ModelAndView agregar(ModelAndView modelAndView) {
        modelAndView.setViewName("admin/pages/Usuario/UsuariosAdd");
        modelAndView.addObject("usuario",new UsuarioDTO());
        return modelAndView;
    }
	
	@RequestMapping("/UsuariosEdit/{id}")
    public ModelAndView editar(@PathVariable long id, ModelAndView modelAndView) {
        modelAndView.setViewName("admin/pages/Usuario/UsuariosEdit");
        modelAndView.addObject("usuario",new UsuarioDTO());
        return modelAndView;
    }
	
	@RequestMapping("/saveUsuario")
    public ModelAndView guardar(@Validated UsuarioDTO usuarioDto, ModelAndView modelAndView) {
		usuarioAppService.nuevoUsuario(usuarioDto);
        modelAndView.setViewName("admin/pages/Usuario/Usuarios");
        modelAndView.addObject("usuarios", usuarioAppService.buscarUsuarios());
        return modelAndView;
    }
	
}
