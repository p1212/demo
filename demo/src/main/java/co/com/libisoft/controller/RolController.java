package co.com.libisoft.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RolController {
	@RequestMapping("/Roles")
    public ModelAndView usuarios(ModelAndView modelAndView) {
        modelAndView.setViewName("admin/pages/Roles");
//        modelAndView.addObject("name", "Yo soy Feng Xuechao");
        return modelAndView;
    }

}
